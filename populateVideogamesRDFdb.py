# required libraries
import pandas as pd
import os
import re
from pathlib import Path


# parameters and URLs
path = str(Path(os.path.abspath(os.getcwd())).parent.absolute())
videogamesUrl = path + '/database-runners/datasets/videogames_sales.csv'
companiesUrl = path + '/database-runners/datasets/videogames companies.csv'
platformUrl = path + '/database-runners/datasets/platform'
genreUrl = path + '/database-runners/datasets/videogames_genre'
countriesUrl = path + '/database-runners/datasets/country_info.csv'
seriesUrl = path + '/database-runners/datasets/Series_videogames.csv'
charactersUrl = path + '/database-runners/datasets/Character_role.csv'
awardsUrl = path + '/database-runners/datasets/game_awards'
# saving folder
savePath = path + '/database-runners/Turtle_output/'


# Load the CSV files in memory
videogames = pd.read_csv(videogamesUrl, sep=',', index_col="index_videogames")
companies = pd.read_csv(companiesUrl, sep=',',index_col="index_companies", keep_default_na=False, na_values=['_'])
platforms = pd.read_csv(platformUrl, sep=',',index_col="index_platform", keep_default_na=False, na_values=['_'])
countries = pd.read_csv(countriesUrl, sep=',',index_col="index_country", keep_default_na=False, na_values=['_'])
characters = pd.read_csv(charactersUrl, sep=',',index_col="index_character_role", keep_default_na=False, na_values=['_'])
readseries = pd.read_csv(seriesUrl, sep=',',index_col="index_series", keep_default_na=False, na_values=['_'])
# we need to convert NaN values to something else otherwise NA strings are converted to NaN
awards = pd.read_csv(awardsUrl, sep=',',keep_default_na=False, na_values=['_'],index_col="index_awards")
genres = pd.read_csv(genreUrl, sep=',',keep_default_na=False, na_values=['_'],index_col="index_genre")

# cast year to int. If type(year) = str --> Literal= year-01-01
companies.astype({'Establish': 'int64'}).dtypes
platforms.astype({'Release Year': 'int64'}).dtypes
awards.astype({'Year': 'int64'}).dtypes


# videogames.info()
# companies.info()
# platform.info()
# countries.info()
# characters.info()
# awards.info()
# genre.info()
# series.info()


# Load the required libraries
from rdflib import Graph, Literal, RDF, URIRef, Namespace
# rdflib knows about some namespaces, like FOAF
from rdflib.namespace import FOAF, XSD

# Construct the videogame ontology namespaces not known by RDFlib
VG = Namespace("http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#")

# create the graph
g = Graph()

# Bind the namespaces to a prefix for more readable output
g.bind("xsd", XSD)
g.bind("vg", VG)

# CHECK DATE
# import datetime

# measure execution time

# iterate over the videogames_sales dataframe
for index, row in videogames.iterrows():
    game = URIRef(VG[index])
    g.add((game, RDF.type, VG.Game))
    g.add((game, VG['originalTitle'], Literal(row['Name'], datatype=XSD.string)))
    g.add((game, VG['year_of_release'], Literal(row['Year_of_Release'], datatype=XSD.integer)))
    g.add((game, VG['NA_Sales'], Literal(row['NA_Sales'], datatype=XSD.double)))
    g.add((game, VG['EU_Sales'], Literal(row['EU_Sales'], datatype=XSD.double)))
    g.add((game, VG['JP_Sales'], Literal(row['JP_Sales'], datatype=XSD.double)))
    g.add((game, VG['Other_Sales'], Literal(row['Other_Sales'], datatype=XSD.double)))
    g.add((game, VG['Global_Sales'], Literal(row['Global_Sales'], datatype=XSD.double)))
    g.add((game, VG['Critic_Score'], Literal(row['Critic_Score'], datatype=XSD.double)))
    g.add((game, VG['Developer'], Literal(row['Developer'], datatype=XSD.string)))
    g.add((game, VG['Rating'], Literal(row['Rating'], datatype=XSD.string)))
    g.add((game, VG['Publisher'], Literal(row['Publisher'], datatype=XSD.string)))

    if (genres["Genre"]==row["Genre"]).any() == True:
        genre = URIRef(VG[genres[genres["Genre"]==row["Genre"]].index[0]])
        g.add((game, VG['hasGenre'], genre))

    if (awards["Nominee"] == row["Name"]).any() == True:
        Nominee = URIRef(VG[awards[awards["Nominee"] == row["Name"]].index[0]])
        g.add((game, VG['nominated'], Nominee))
        if (awards['Winner'] == '1').any() == True:
            g.add((game, VG['winner'], Nominee))

    if (platforms["Abbrev"] == row["Platform"]).any() == True:
        Abbrev = URIRef(VG[platforms[platforms["Abbrev"] == row["Platform"]].index[0]])
        g.add((game, VG['runIn'], Abbrev))

    '''Series = URIRef(VG[series[series['FranchiseName'] == row['Name']]])
    g.add((game, VG['isPartOf'], Series))
    characters = URIRef(VG[characters[characters['CharacterName'] == row['Name']]])
    g.add((game, VG['hasCharacter'], characters))'''

# print all the data in the Turtle format
print("--- saving serialization ---")

with open(savePath + 'videogames_tmp.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

reading_file = open(savePath + "videogames_tmp.ttl", "r")

new_file_content = ""
for line in reading_file:
  stripped_line = line.strip()
  new_line = stripped_line.replace("year_of_release nan", "year_of_release 0")
  new_file_content += new_line +"\n"
reading_file.close()

writing_file = open(savePath + "videogames.ttl", "w")
writing_file.write(new_file_content)
writing_file.close()

os.remove(savePath + "videogames_tmp.ttl")




# Bind the namespaces to a prefix for more readable output

g= Graph()
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the company dataframe
for index, row in companies.iterrows():
    company = URIRef(VG[index])
    g.add((company, RDF.type, VG.Company))
    g.add((company, VG['name_company'], Literal(row['Developer'], datatype=XSD.string)))
    g.add((company, VG['city'], Literal(row['City'], datatype=XSD.string)))
    g.add((company, VG['date_est'], Literal(row['Establish'], datatype=XSD.integer)))
    g.add((company, VG['notable_things'], Literal(row['Notable games series or franchises'], datatype=XSD.string)))

    if (videogames["Publisher"] == row["Developer"]).any() == True:
        Publisher = URIRef(VG[videogames[videogames["Publisher"] == row["Developer"]].index[0]])
        g.add((company, VG['hasPublished'], Publisher))

    if (videogames["Developer"] == row["Developer"]).any() == True:
        Developer = URIRef(VG[videogames[videogames["Developer"] == row["Developer"]].index[0]])
        g.add((company, VG['hasDeveloped'], Developer))

    countrys = row['Country']
    if (countries["Country"] == countrys).any():
        country = URIRef(VG[countries[countries["Country"] == row["Country"]].index[0]])
        g.add((company, VG['hasCountry'], country))

print("--- saving serialization ---")
with open(savePath + 'companies.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

# create the graph

g= Graph()


# Bind the namespaces to a prefix for more readable output
# g.bind("foaf", FOAF)
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the platform dataframe
for index, row in platforms.iterrows():
    platform = URIRef(VG[index])
    g.add((platform, RDF.type, VG.Platform))
    g.add((platform, VG['name_platform'], Literal(row['Platform'], datatype=XSD.string)))
    g.add((platform, VG['abbreviation'], Literal(row['Abbrev'], datatype=XSD.string)))
    g.add((platform, VG['company_platform'], Literal(row['Company'], datatype=XSD.string)))
    g.add((platform, VG['platform_release_date'], Literal(row['Release Year'], datatype=XSD.integer)))

print("--- saving serialization ---")
with open(savePath + 'platform.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

# create the graph
g = Graph()


# Bind the namespaces to a prefix for more readable output
# g.bind("foaf", FOAF)
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the countries dataframe
for index, row in countries.iterrows():
    country = URIRef(VG[index])
    g.add((country, RDF.type, VG.Countries))
    g.add((country, VG['name_country'], Literal(row['Country'], datatype=XSD.string)))
    g.add((country, VG['region'], Literal(row['Region'], datatype=XSD.string)))

print("--- saving serialization ---")
with open(savePath + 'countries.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

# create the graph
g = Graph()


# Bind the namespaces to a prefix for more readable output
# g.bind("foaf", FOAF)
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the awards dataframe
for index, row in awards.iterrows():
    awards = URIRef(VG[index])
    g.add((awards, RDF.type, VG.awards))
    g.add((awards, VG['Years'], Literal(row['Year'], datatype=XSD.integer)))
    g.add((awards, VG['Category'], Literal(row['Category'], datatype=XSD.string)))
    g.add((awards, VG['Nominee'], Literal(row['Nominee'], datatype=XSD.string)))
    g.add((awards, VG['Company'], Literal(row['Company'], datatype=XSD.string)))
    g.add((awards, VG['Winner'], Literal(row['Winner'], datatype=XSD.integer)))
    g.add((awards, VG['Voted'], Literal(row['Voted'], datatype=XSD.string)))

print("--- saving serialization ---")
with open(savePath + 'awards.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

# create the graph
g = Graph()


# Bind the namespaces to a prefix for more readable output
# g.bind("foaf", FOAF)
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the characters dataframe
for index, row in characters.iterrows():
    characters = URIRef(VG[index])
    Series = URIRef(VG[row['Series']])
    g.add((characters, RDF.type, VG.characters))
    g.add((characters, VG['character_name'], Literal(row['CharacterName'], datatype=XSD.string)))
    g.add((characters, VG['role'], Literal(row['Role'], datatype=XSD.string)))
    for gm in row['Games'].split(';'):
        Games = URIRef(VG[gm.strip()])
        g.add((characters, VG['isIn'], Games))
    g.add((characters, VG['aCharactersOf'], Series))

    Series = URIRef(VG[readseries[readseries['FranchiseName'] == row['CharacterName']]])
    '''g.add((characters, VG['partecipatedIn'], Series))'''


print("--- saving serialization ---")
with open(savePath + 'characters.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

# create the graph
g = Graph()


# Bind the namespaces to a prefix for more readable output
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the genre dataframe
for index, row in genres.iterrows():
    genre = URIRef(VG[index])
    g.add((genre, RDF.type, VG.Genre))
    g.add((genre, VG['title_genre'], Literal(row['Genre'], datatype=XSD.string)))
    g.add((genre, VG['explanation'], Literal(row['Explanation'], datatype=XSD.string)))
    g.add((genre, VG['examples'], Literal(row['Examples'], datatype=XSD.string)))

print("--- saving serialization ---")
with open(savePath + 'genre.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))

# create the graph
g = Graph()

# Bind the namespaces to a prefix for more readable output
# g.bind("foaf", FOAF)
g.bind("xsd", XSD)
g.bind("vg", VG)

# iterate over the series dataframe
for index, row in readseries.iterrows():
    series = URIRef(VG[index])
    g.add((series, RDF.type, VG.Series))
    g.add((series, VG['franchise_name'], Literal(row['FranchiseName'], datatype=XSD.string)))
    g.add((series, VG['description'], Literal(row['Description'], datatype=XSD.string)))
    for gm in row['Games'].split(';'):
        Games = URIRef(VG[gm.strip()])
        g.add((series, VG['hasGames'], Games))


print("--- saving serialization ---")
with open(savePath + 'series.ttl', 'w') as file:
    file.write(g.serialize(format='turtle'))



