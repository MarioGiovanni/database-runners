QUERY 1: All the gameS for company founded in a year that winner an award

PREFIX vg:<http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?name_company ?year_foundation ?name_game ?year_win ?category WHERE {
    
    ?company vg:name_company ?name_company .
    ?company vg:date_est ?year_foundation .
    ?company vg:hasDeveloped ?game .
    ?game vg:nominated ?awards .
    ?awards vg:Nominee ?name_game .
    ?awards vg:Category ?category .
    ?awards vg:Winner ?winner_esit .
    ?awards vg:Years ?year_win .
    
    FILTER (?year_foundation > 1990 && ?winner_esit = 1)
}



QUERY 2: Sum of the global sales for all the games of a platform in year 2016

PREFIX vg:<http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT (SUM (?global_sale) AS ?Sum_Global_Sales) ?Platform_name ?year WHERE {
    
    ?game vg:runIn ?platform . 
    ?platform vg:name_platform ?Platform_name .
    ?game vg:Global_Sales ?global_sale .
    ?game vg:year_of_release ?year .
    
    Filter Exists{?platform vg:name_platform "Playstation 4"^^xsd:string}.
    Filter(?year = 2016)
}

GROUP BY ?Platform_name ?year



QUERY 3: Genre that sales most

PREFIX vg:<http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?name_genre  (SUM(?global_sales_game) AS ?global_sales) WHERE {
  
	?genre vg:title_genre ?name_genre.
    ?game vg:hasGenre ?genre.
    ?game vg:Global_Sales ?global_sales_game .
    
}
GROUP BY ?name_genre
ORDER BY DESC (?global_sales)
LIMIT 1



QUERY 4: Companies with more nominations

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

SELECT (COUNT(?nomine) AS ?num) ?name_company WHERE {

		?company vg:name_company ?name_company .
		?company vg:hasDeveloped ?game .
		?game vg:nominated ?awards .
    	?awards vg:Nominee ?nomine
		
}

GROUP BY ?name_company
ORDER BY DESC (?num)
LIMIT 1


QUERY 5: Avg jappon sales for all the genres 

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

SELECT  (avg(?jp_sales) AS ?avg_jappon_sales) ?name_genre WHERE {

?genre vg:title_genre ?name_genre .
?game vg:hasGenre ?genre .
?game vg:JP_Sales ?jp_sales .

}

GROUP BY ?name_genre




QUERY 6: Filter from 2001 to 2016 release date of the platforms 

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

SELECT ?name_platform ?date_release WHERE {

?platform vg:name_platform ?name_platform.
  
?platform vg:platform_release_date ?date_release .

FILTER(?date_release > 2001 && ?date_release < 2016)

}


QUERY 7:  List of games with Mario character and their genre

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

SELECT ?character_name ?game_name ?title_genre WHERE {

	?characters vg:character_name ?character_name .
	?characters vg:isIn ?game .
	?game vg:originalTitle ?game_name .
    ?game vg:hasGenre ?genre .
    ?genre vg:title_genre ?title_genre .

FILTER REGEX(?character_name, "Mar*")
	
}


QUERY 8: 

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

SELECT DISTINCT ?name_company ?country_name ?name_game ?critic_score WHERE {
    
    ?company vg:name_company ?name_company .
    ?company vg:hasCountry ?country .
    ?country vg:name_country ?country_name .
    ?game vg:Critic_Score ?critic_score .
    ?game vg:originalTitle ?name_game
    
    FILTER REGEX(?country_name , "Unit*") 
    FILTER(?critic_score > 85)
}

QUERY 9: List of games of a Serie filtered by name of franchise

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

SELECT DISTINCT ?franchise_name ?game_name ?description WHERE {
    
   	?series vg:franchise_name ?franchise_name .
	?series vg:hasGames ?game .
	?series vg:description ?description .
	?game vg:originalTitle ?game_name .
	
    
    FILTER REGEX(?franchise_name , "Ma*") 
}
ORDER BY ?game_name


QUERY 10: Has Kaos Studios produced on 2016 games? 

PREFIX vg: <http://www.semanticweb.org/gabrieledelfiume/ontologies/2021/10/videogames_ontology#>

ASK WHERE {
    
    ?company vg:name_company ?name_company .
    ?company vg:hasDeveloped ?game .
    ?game vg:year_of_release ?year_of_release .
    
    FILTER(?year_of_release = 2016 && ?name_company = "Kaos Studios")
    
}


